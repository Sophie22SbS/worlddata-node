"use strict";
const mon = require("./mongoWrap");
const dbServer = "localhost";
const dbName = "world";

exports.getGovernmentforms = async function (res) {
    try {
        let cs = await mon.retrieve(dbServer, dbName, "governmentform", {});
        res.render('governmentform', {
            title: 'Fragments of the World',
            subtitle: 'Select Governmentform',
            governmentforms: cs
        });
    } catch (e) {
        console.log(e);
    }
}

exports.getGovernmentform = async function (res, ctryname) {
    try {
        let cs = await mon.retrieve(dbServer, dbName, "governmentform", { "name": ctryname });
        res.render('governmentformDisplay', {
            title: 'Fragments of the World',
            subtitle: ctryname,
            governmentforms: cs
        });
    } catch (e) {
        console.log(e);
    }
}

exports.postGovernmentform = async function (req, res, next) {
    let chk = { name: req.body.name };  // check object for existence
    let governmentform = {                     // create obejct in db-format
        name: req.body.name
    };
    if (req.body.localname === "") governmentform.localname = governmentform.name;
    console.log(req.body);
    try {
        let cs = await mon.upsert(dbServer, dbName, "governmentform", governmentform, chk);
        res.redirect("/");
    } catch (e) {
        console.log(e);
    }
}