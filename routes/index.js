const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');                 // added for bcrypt hashing
const sha256 = require('crypto-js/sha256');         // added for encryption and other hashes
const sha512 = require('crypto-js/sha512');         // added for encryption and other hashes
const md5 = require('crypto-js/md5');               // added for encryption and other hashes
const sha1 = require('crypto-js/sha1');             // added for encryption and other hashes
const sha3 = require('crypto-js/sha3');             // added for encryption and other hashes
const saltiter = 10;

const modCountry = require("../models/handleCountries");

router.get('/country', function(req, res, next) {
    modCountry.getCountries(res);
});
router.get('/country/:country', function(req, res, next) {
    modCountry.getCountry(res, req.params.country);
});
router.post("/country", function(req, res, next) {
    modCountry.getCountry(res, req.body.ctry);
});

router.get('/countryData', function(req, res, next) {
    res.render('countryData', {
        title: 'Fragments of the World',
        subtitle: 'Enter Country Data'
    });
});
router.post("/countryData", function(req, res, next) {
    modCountry.postCountry(req, res, next);
});

/*-----------*/

const modGovernmentform = require("../models/handleGovernmentforms");

router.get('/governmentform', function(req, res, next) {
    modGovernmentform.getGovernmentforms(res);
});
router.get('/governmentform/:governmentform', function(req, res, next) {
    modGovernmentform.getGovernmentform(res, req.params.governmentform);
});
router.post("/governmentform", function(req, res, next) {
    modGovernmentform.getGovernmentform(res, req.body.ctry);
});

router.get('/governmentformData', function(req, res, next) {
    res.render('governmentformData', {
        title: 'Fragments of the World',
        subtitle: 'Enter governmentform Data'
    });
});
router.post("/governmentformData", function(req, res, next) {
    modGovernmentform.postGovernmentform(req, res, next);
});

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { 
        title: 'Fragments of the World',
        subtitle: 'Display and Register World Data'
    });
});
router.get('/testhash', function(req, res, next) {
    res.render('testhash', { title: 'Express' });
});
router.post('/testhash', async function(req, res, next) {
    let plain = req.body.password;
    let digests = [];
    let digobj;
    digobj = await bchash(req);
    digests.push(digobj);
    digobj = await bchash(req);
    digests.push(digobj);
    digobj = await bchash(req);
    digests.push(digobj);
    digobj = await bchash(req);
    digests.push(digobj);
    digobj = await bchash(req, 'md5');
    digests.push(digobj);
    digobj = await bchash(req, 'md5');
    digests.push(digobj);
    digobj = await bchash(req, 'sha1');
    digests.push(digobj);
    digobj = await bchash(req, 'sha1');
    digests.push(digobj);
    digobj = await bchash(req, 'sha3');
    digests.push(digobj);
    digobj = await bchash(req, 'sha3');
    digests.push(digobj);
    digobj = await bchash(req, 'sha256');
    digests.push(digobj);
    digobj = await bchash(req, 'sha256');
    digests.push(digobj);
    digobj = await bchash(req, 'sha512');
    digests.push(digobj);
    digobj = await bchash(req, 'sha512');
    digests.push(digobj);
    res.render('testhash', {
        title: 'Express',
        pwd: req.body.password,
        digests
    });
});

const bchash = async function (req, algo='bcrypt') {
    let hash;
    switch (algo) {
        case 'md5':
            hash = await String(md5(req.body.password));
            break;
        case 'sha1':
            hash = await String(sha1(req.body.password));
            break;
        case 'sha3':
            hash = await String(sha3(req.body.password));
            break;
        case 'sha256':
            hash = await String(sha256(req.body.password));
            break;
        case 'sha512':
            hash = await String(sha512(req.body.password));
            break;
        default:
            hash = await bcrypt.hash(req.body.password, 10);
            break;
    }
    let obj = {
        algo: algo,
        length: hash.length,
        digest: hash
    };
    return obj;
} 
module.exports = router;
