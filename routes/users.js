const express = require('express');
const router = express.Router();
const models = require('../models/models.js');
const controllers = require('../controllers/login.js');

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

router.get('/reguser', function(req, res, next) {
    res.render('reguser', { title: 'Register User' });
});
router.post('/reguser', async function(req, res, next) {
	models.putUser(req)
		.then ( function (rc) {
			if (!rc)
				res.render('reguser', { title: 'Register User', returnCode: rc });
			else	
				res.redirect('/');
		});
});

router.get('/dispuser', async function(req, res, next) {
	models.getUsers()
		.then( function (userarr) {
			res.render('dispusers', { title: 'Display Users', userarr });
		});
});

router.get('/login', function(req, res, next) {
    res.render('login', { title: 'Login' });
});
router.post('/login', async function(req, res, next) {
	controllers.login(req)
		.then( function (rc) {
			if (!rc)
				res.render('login', { title: 'Login', tf: "misery", returnCode: rc });
			else	
				res.render('login', { title: 'Login', tf: "success",  returnCode: rc });
		});
});

module.exports = router;
